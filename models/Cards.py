faces_dict = {
    3: '3',
    4: '4',
    5: '5',
    6: '6',
    7: '7',
    8: '8',
    9: '9',
    10: '10',
    11: 'Valet',
    12: 'Dame',
    13: 'Roi',
    14: 'A',
    15: '2',
}

class Card:
    def __init__(self, value, face ):
        self.value = value
        self.face = face

    def __gt__(self, other):
        print(self)
        return self.value > other.value

    def __str__(self):
        return f'je suis : {self.value_to_name()} {self.face}'

    def value_to_name(self):
            return faces_dict[self.value]

